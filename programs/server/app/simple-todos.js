(function(){//how to set session on window load
//server part
Tasks = new Mongo.Collection("tasks");
Users = new Mongo.Collection("user");
if (Meteor.isClient) {
  // This code only runs on the client
  Template.body.helpers({

  });

  Template.body.events({
    "click .set": function(){
      Session.set("page","set");
    },

    "click .user": function(){
      Session.set("page","user");
      Session.set("emailErr","");
      Session.set("nameErr","");
      Session.set("deadlineErr","");
    },

    "click .task": function(){
      Session.set("page","task");
      Session.set("emailErr","");
      Session.set("nameErr","");
      Session.set("deadlineErr","");
      Session.set("offset",0);
      Session.set("filter","all");
      Session.set("order","deadline");
      Session.set("sort","inc");
    }
  });
//------------------all----------------------//
  Template.all.helpers({
    Isset: function(){
      var temp = Session.get("page");
      return temp==="set";
    },

    Isuser: function(){
      var temp = Session.get("page");
      return temp==="user";
    },

    Istask: function(){
      var temp = Session.get("page");
      return temp==="task";
    },

    Isadduser: function(){
      var temp = Session.get("page");
      return temp==="adduser";
    },

    Isaddtask: function(){
      var temp = Session.get("page");
      return temp==="addtask";
    },

    Isuserinfo: function(){
      var temp = Session.get("page");
      return temp==="userinfo";
    },

    Istaskinfo: function(){
      var temp = Session.get("page");
      return temp==="taskinfo";
    },

    Isupdatetask: function(){
      var temp = Session.get("page");
      return temp === "updatetask";
    }
  });
  
//------------------user----------------------//
  Template.user.helpers({
    users: function(){
      return Users.find({});
    }
  });
  Template.user.events({
    "click .add_user": function(){
      Session.set("page","adduser");
    },

    "click .delete_user": function(){
      var user = Users.findOne({_id:this._id});
      for(var i=0; i<user.pendingTasks.length; i++){
        Tasks.remove(user.pendingTasks[i]);
      }
      Users.remove(this._id);
    },

    "click .show_user_info": function(){
      Session.set("user_id",this._id);
      Session.set("page","userinfo");
    }
  });
//------------------adduser----------------------//
  Template.adduser.helpers({
    nameErr: function(){
      return Session.get("nameErr");
    },
    emailErr: function(){
      return Session.get("emailErr");
    }
  }),

  Template.adduser.events({
    "submit .user_submit": function(event){
      var flag = true;
      var user_name = event.target.name.value;
      var user_email = event.target.email.value;
      if(!user_name){
        Session.set("nameErr","User Name Required");
        flag = false;
      }

      var re = /\S+@\S+\.\S+/;
      if(!re.test(user_email)){
        Session.set("emailErr","Invalid email");
        flag = false;
      }

      if(!user_email){
        Session.set("emailErr","Email Required");
        flag = false;
      }

      //check if repeated email
      if(Users.findOne({email: user_email})){
        Session.set("emailErr","email already existed");
        flag = false;
      }

      if(flag){
      Users.insert({
        name: user_name,
        email: user_email,
        pendingTasks: [],
        createdAt: new Date()
      });
      Session.set("emailErr","");
      Session.set("nameErr","");
      }
      event.target.name.value="";
      event.target.email.value="";
      return false;
    }
  });
//------------------userinfo----------------------//
  Template.userinfo.helpers({
    user: function(){
      var id = Session.get("user_id");
      var user = Users.findOne({_id: id});//user findOne OK, use find not OK, why?
      return user;
    },
    pendingtasks: function(){
      var id = Session.get("user_id");
      var user = Users.findOne({_id: id});
      var pendingTasks = user.pendingTasks;
      var ret=[];
      for(var i=0; i<pendingTasks.length; i++){
        var temp = Tasks.findOne({_id:pendingTasks[i]});
        if(!temp.completed)
          ret.push(temp);
      }
      return ret;
    },
    completedtasks: function(){
      var id = Session.get("user_id");
      var user = Users.findOne({_id: id});

      var pendingTasks = user.pendingTasks;
      var ret=[];
      for(var i=0; i<pendingTasks.length; i++){
        var temp = Tasks.findOne({_id:pendingTasks[i]});
        if(temp.completed)
          ret.push(temp);
      }
      return ret;
    }
  });

  Template.userinfo.events({
    "click .complete": function(){
      Tasks.update(this._id,{$set:{completed: true}});
    },

    "click .show_complete_tasks": function(){
      if(document.getElementById("completed_tasks").className=="show"){
        document.getElementById("completed_tasks").className="hide";
      }
      else{
        document.getElementById("completed_tasks").className="show";
      }
    },

    "click .show_task_info": function(){
      Session.set("task_id",this._id);
      Session.set("page","taskinfo");
    }
  });
//------------------task----------------------//
  Template.task.helpers({
    tasks: function(){
      var temp;
      if(Session.get('filter')=="all"){
        if(Session.get('order')=="deadline"){
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({},{sort: {deadline: 1}}).fetch();
          }
          else{
            temp = Tasks.find({},{sort: {deadline: -1}}).fetch();
          }
        }
        else{
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({},{sort: {assignedUserName: 1}}).fetch();
          }
          else{
            temp = Tasks.find({},{sort: {assignedUserName: -1}}).fetch();
          }
        }
      }

      else if(Session.get('filter')=="pendingTasks"){
        if(Session.get('order')=="deadline"){
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({completed:false},{sort: {deadline: 1}}).fetch();
          }
          else{
            temp = Tasks.find({completed:false},{sort: {deadline: -1}}).fetch();
          }
        }
        else{
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({completed:false},{sort: {assignedUserName: 1}}).fetch();
          }
          else{
            temp = Tasks.find({completed:false},{sort: {assignedUserName: -1}}).fetch();
          }
        }
      }
      else{
        if(Session.get('order')=="deadline"){
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({completed:true},{sort: {deadline: 1}}).fetch();
          }
          else{
            temp = Tasks.find({completed:true},{sort: {deadline: -1}}).fetch();
          }
        }
        else{
          if(Session.get('sort')=="inc"){
            temp = Tasks.find({completed:true},{sort: {assignedUserName: 1}}).fetch();
          }
          else{
            temp = Tasks.find({completed:true},{sort: {assignedUserName: -1}}).fetch();
          }
        }
      }
      var ret = [];
      var len;
      if(Session.get("offset")+3 >= temp.length){
        len = temp.length;
      }
      else{
        len = Session.get("offset")+3;
      }

      for(var i=Session.get("offset");i<len;i++){
        ret.push(temp[i]);
      }
      Session.set("totalTasks",temp);
      return ret;
    }
  });

  Template.task.events({
    "click .show_task_info": function(){
      Session.set("task_id",this._id);
      Session.set("page","taskinfo");
    },

    "click .add_task": function(){
      Session.set("page","addtask");
    },

    "click .delete_task": function(){
      var task = Tasks.findOne({_id:this._id});
      var user = Users.findOne({_id:task.assignedUser});
      var index = user.pendingTasks.indexOf(this._id);
      user.pendingTasks.splice(index,1);
      Users.update(task.assignedUser,{$set:{pendingTasks:user.pendingTasks}});
      Tasks.remove(this._id);
    },

    "click .filter": function(){
      Session.set("filter",$('input:radio[name=filter]:checked').val());
    },

    "click .sort":function(){
      Session.set("sort",$('input:radio[name=sort]:checked').val());
    },

    "change .order": function(event){
      Session.set("order",event.target.value);
    },

    "click .prev": function(){
      if(Session.get("offset")-3>=0){
        var temp = Session.get("offset");
        Session.set("offset",temp-3);
      }
    },

    "click .next": function(){
      if(Session.get("offset")+3<Session.get("totalTasks").length){
        var temp = Session.get("offset");
        Session.set("offset",temp+3);
      }
    }

  });
//------------------addtask----------------------//
  Template.addtask.helpers({
    users: function(){
      return Users.find({});
    },
    nameErr: function(){
      return Session.get("nameErr");
    },
    deadlineErr: function(){
      return Session.get("deadlineErr");
    }
  });

  Template.addtask.events({
    "submit .task_submit": function(event){
      //add task to Tasks
      var task_name = event.target.name.value;
      var task_description = event.target.description.value;
      if(!task_description) task_description = "No Description";
      var task_deadline = event.target.deadline.value;
      var task_assigneduser = event.target.selected_assigneduser.value;
      var task_assignedusername = Users.findOne({_id:task_assigneduser}).name;
      var flag = true;

      if(!task_name){
        Session.set("nameErr","Task Name Required");
        flag = false;
      }

      if(!task_deadline){
        Session.set("deadlineErr","Deadline Required");
        flag = false;
      }

      if(flag){
      var id = Tasks.insert({
        name: task_name,
        description: task_description,
        deadline: task_deadline,
        completed: false,
        assignedUser: task_assigneduser,
        assignedUserName: task_assignedusername,
        createdAt: new Date()
      });

      //add task to the user
      var user = Users.findOne({_id:task_assigneduser});
      user.pendingTasks.push(id);
      Users.update(task_assigneduser,{$set:{pendingTasks: user.pendingTasks}});

      Session.set("nameErr","");
      Session.set("deadlineErr","");
      }

      event.target.name.value = "";
      event.target.description.value = "";
      event.target.deadline.value = "";

      return false;
    }
  });
//------------------taskinfo----------------------//
  Template.taskinfo.helpers({
    "task": function(){
      return Tasks.findOne({_id:Session.get("task_id")});
    }
  });

  Template.taskinfo.events({
    "click .update_task": function(){
      Session.set("page","updatetask");
    }
  });
//------------------updatetask----------------------//
  Template.updatetask.helpers({
    users: function(){
      return Users.find({});
    }
  });

  Template.updatetask.events({
    "submit .task_update": function(event){
      //delete task from old user
      var task = Tasks.findOne({_id:Session.get("task_id")});
      var user = Users.findOne({_id:task.assignedUser});
      var index = user.pendingTasks.indexOf(task._id);
      user.pendingTasks.splice(index,1);
      Users.update(user._id,{$set:{pendingTasks:user.pendingTasks}});

      //add task to new user
      var newuser = Users.findOne({_id:event.target.selected_assigneduser.value});
      newuser.pendingTasks.push(Session.get("task_id"));
      Users.update(event.target.selected_assigneduser.value,{$set: {pendingTasks: newuser.pendingTasks}});

      var new_task = {};
      if(event.target.name.value) new_task.name = event.target.name.value;
      if(event.target.description.value) new_task.description = event.target.description.value;
      if(event.target.deadline.value) new_task.deadline = event.target.deadline.value;
      new_task.assignedUser = event.target.selected_assigneduser.value;
      new_task.assignedUserName = Users.findOne({_id:event.target.selected_assigneduser.value}).name;
      Tasks.update(Session.get("task_id"),{$set:new_task});


      event.target.name.value = "";
      event.target.description.value = "";
      event.target.deadline.value = "";
      return false;
    }
  })
}

})();
